module github.com/iam-veeramalla/cicd-demo-golang

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
)
